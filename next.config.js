const path = require('path');
const webpack = require('webpack');

module.exports = {
  webpack: (config, options) => {
    config.resolve.alias['components'] = path.resolve(__dirname)+"/components";
    config.resolve.alias['resources'] = path.resolve(__dirname)+"/resources";
    config.resolve.alias['config'] = path.resolve(__dirname)+"/config";
    return config;
  }
};
