import Head from 'next/head'
import ProductsList from 'components/productsList';

export default function Home() {
  return (
    <ProductsList />
  )
}
