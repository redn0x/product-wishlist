import { blue, green, orange, red } from '@material-ui/core/colors';

import { createMuiTheme } from '@material-ui/core/styles';

// Create a theme instance.
const theme = createMuiTheme({
  palette: {
    primary: blue,
    secondary: green,
    tertiary: orange,
    error: red,
    background:{ default:"#fff"},
    text: {
      primary: 'rgba(0, 0, 0, 0.87)',
      secondary: 'rgba(0, 0, 0, 0.54)',
      disabled: 'rgba(0, 0, 0, 0.38)',
      hint: 'rgba(0, 0, 0, 0.38)',
      icon: 'rgba(0, 0, 0, 0.38)',
      divider: 'rgba(0, 0, 0, 0.12)',
      lightDivider: 'rgba(0, 0, 0, 0.075)',
    },
  },
  typography: {
    fontFamily: "'Open Sans', 'Roboto', sans-serif",
    useNextVariants: true,
  },
  breakpoints: {
    forPhoneOnly: `@media (max-width: 599px)`,
    forTabletPortraitDown: `@media (max-width: 600px)`,
    forTabletPortraitUp: `@media (min-width: 600px)`,
    forTabletLandscapeDown: `@media (max-width: 900px)`,
    forTabletLandscapeUp: `@media (min-width: 900px)`,
    forBigTabletPortraitDown: `@media (max-width: 769px)`,
    forBigTabletPortraitUp: `@media (min-width: 769px)`,
    forBigTabletLandscapeDown: `@media (max-width: 1025px)`,
    forBigTabletLandscapeUp: `@media (min-width: 1025px)`,
    forDesktopDown: `@media (max-width: 1200px)`,
    forDesktopUp: `@media (min-width: 1200px)`,
    forBigDesktopUp: `@media (min-width: 1800px)`,
  },
  animation: {
    duration: 0.3,
  },
});

export default theme;
