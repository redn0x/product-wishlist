
const Reducer = ( state, action ) => {
  switch (action.type) {
    case "ADD_TO_WISHLIST":{
      const { product } = action;
      return {
        ...state,
        wishlist: {
          ...state.wishlist,
          [product._id]:{
            ...product,
            quantity:1
          }
        }
      };
    }
    case "REMOVE_FROM_WISHLIST":{
      const { product } = action;
      let wishlist = {...state.wishlist};
      delete wishlist[product._id]
      return { ...state, wishlist };
    }
    case "CHANGE_WISHLIST_ITEM_QUANTITY":{
      const quantity = action.quantity >= 0?action.quantity:0;
      const item = { ...action.item, quantity };
      const wishlist = {...state.wishlist, [item._id]:item};
      return { ...state, wishlist };
    }
    case "SET_WISHLIST":{
      return { ...state, wishlist:action.wishlist };
    }
    default: return state;
  }
}

export default Reducer;
