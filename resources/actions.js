export const addToWishlist = (product) => ({
  type: "ADD_TO_WISHLIST",
  product
})

export const removeFromWishlist = (product) => ({
  type: "REMOVE_FROM_WISHLIST",
  product
})

export const changeWishlistItemQuantity = (item, quantity) => ({
  type: "CHANGE_WISHLIST_ITEM_QUANTITY",
  item, quantity
})

export const setWishlist = (wishlist) => ({
  type: "SET_WISHLIST",
  wishlist
})
