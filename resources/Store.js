import { setWishlist } from './actions';
import { createContext, useReducer, useEffect } from 'react';
import products from './sampleData';

import Reducer from './Reducer';

const initialState = {
  wishlist:{},
  products
}

let wishlistLoaded = false;

const Store = ({children}) => {
  const [ state, dispatch ] = useReducer(Reducer, initialState)

  //load wishlist from localstorage
  useEffect(()=>{
    const wishlistJson = localStorage.getItem("wishlist");
    if(wishlistJson !== null){
      const wishlist = JSON.parse(wishlistJson);
      dispatch(setWishlist(wishlist));
    }

    wishlistLoaded = true;

    //this will keep multiple tabs and windows synchronized
    window.addEventListener('storage', e => {
      if(e.key === 'wishlist') {
        const wishlist = JSON.parse(e.newValue);
        dispatch(setWishlist(wishlist));
      }
    });
  },[]);

  //save wishlist to localstate
  useEffect(()=>{
    if(wishlistLoaded)
      localStorage.setItem("wishlist", JSON.stringify(state.wishlist));
  },[state.wishlist])

  return (
    <Context.Provider value={[state, dispatch]}>
      {children}
    </Context.Provider>
  );
}

export const Context = createContext(initialState);

export default Store;
