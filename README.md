# Product Wishlist
## Demo
[https://cedira.biz:1259/](https://cedira.biz:1259/)

## About
The Product Wishlist is build using NextJS ( React Framework for Production)
and for the UI components Material-UI was used.

### Important file locations
- components/*
- resources/*
- pages/_app.js
- pages/_document.js
- pages/index.js

### Commands
To run developent version from commandline:
> npm run dev

To run production build from commandline:
> npm run build
> npm run start

## Docker
This application is available as a docker image

Pull:
> docker pull redn0x/product-wishlist

Run on port 3000
> docker run -it --rm -p3000:3000 redn0x/product-wishlist
