FROM node:13-alpine
WORKDIR /app
COPY ./ /app

RUN npm install -g npm   \
    && npm install   \
    && npm update   \
    && npm run build

EXPOSE 3000

CMD npm start
