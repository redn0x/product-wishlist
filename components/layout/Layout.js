import Container from '@material-ui/core/Container';
import Header from './Header';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  "@global":{
    "html, body, body > div:first-child, div#__next, div#__next > div":{
      height:"100%"
    }
  },
  root: {
    display: "grid",
    gridRowGap:10,
    gridTemplateRows: "64px 1fr"
  }
}));

const Layout = ({children}) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Header />
      <Container>
      {children}
      </Container>
    </div>
  );
}

export default Layout;
