import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import FavoriteIcon from '@material-ui/icons/Favorite';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Wishlist from 'components/wishlist';
import { makeStyles } from '@material-ui/core/styles';
import { useState } from 'react';
import { useContext } from "react";
import { Context } from "resources/Store";

const useStyles = makeStyles(() => ({
  root: {
    flexGrow: 1,
  },
  logo: {
    flexGrow: 1,
    fontWeight:"bold"
  },
  wishlistButton: {
    color:"#fff",
    fontWeight:"bold"
  }
}));

const Header = () => {
  const classes = useStyles();
  const [ state ] = useContext(Context);
  const [ wishlistOpen, setWishlistOpen ] = useState(false)
  const wishlistItemsSize = Object.values(state.wishlist).length;

  return (
    <div className={classes.root}>
      <AppBar>
        <Container>
          <Toolbar>
            <Typography variant="h6" component="h1" className={classes.logo}>Blenders</Typography>
            <Button
              className={classes.wishlistButton}
              onClick={()=>{setWishlistOpen(true)}}
            >
              Wishlist&nbsp;
              {wishlistItemsSize?
                <div>({wishlistItemsSize})&nbsp;</div>
                :false
              }
              <FavoriteIcon/>
            </Button>
          </Toolbar>
        </Container>
      </AppBar>
      <Wishlist
        open={wishlistOpen}
        onClose={()=>{setWishlistOpen(false)}}
      />
    </div>
  );
}

export default Header;
