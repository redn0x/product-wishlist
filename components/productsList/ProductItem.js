import { addToWishlist, removeFromWishlist } from 'resources/actions';

import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import { Context } from "resources/Store";
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import FavoriteIcon from '@material-ui/icons/Favorite';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import red from '@material-ui/core/colors/red';
import { useContext } from "react";

const useStyles = makeStyles(() => ({
  root: {
    display:"flex",
    flexDirection: "column"
  },
  media:{
    height:150,
    backgroundSize: "contain"
  },
  content:{
    flexGrow:1
  },
  favorite:{
    color: red[500]
  }
}));

const ProductItem = ({product}) => {
  const classes = useStyles();
  const [state, dispatch] = useContext(Context);
  const isInWishlist = Boolean(state.wishlist[product._id]);

  const handleAddToWishlist = () => {
    dispatch(addToWishlist(product));
  }

  const handleRemoveFromWishlist = () => {
    dispatch(removeFromWishlist(product));
  }

  return (
    <Card className={classes.root}>
      <CardMedia
        className={classes.media}
        image={product.img}
        title={product.name}
      />
      <CardContent className={classes.content}>
        <Typography gutterBottom variant="h6" component="h2">
          {product.name}
        </Typography>
      </CardContent>

      <CardActions>
        { isInWishlist?(
            <Button color="primary" onClick={handleRemoveFromWishlist}>
              <FavoriteIcon className={classes.favorite} />&nbsp;Remove from wishlist
            </Button>
          ):(
            <Button onClick={handleAddToWishlist} fullWidth>
              <FavoriteBorderIcon className={classes.favorite} />&nbsp;Add to wishlist
            </Button>
          )
        }
      </CardActions>
    </Card>
  );
}

export default ProductItem;
