import { Context } from "resources/Store";
import ProductItem from "./ProductItem";
import { makeStyles } from '@material-ui/core/styles';
import { useContext } from "react";

const useStyles = makeStyles(theme => ({
  root: {
    display: "grid",
    gridGap:20,
    gridTemplateColumns: "1fr 1fr 1fr 1fr 1fr"
  },
  [theme.breakpoints.forBigTabletLandscapeDown]:{
    root: {
      gridTemplateColumns: "1fr 1fr 1fr"
    }
  },
  [theme.breakpoints.forTabletLandscapeDown]:{
    root: {
      gridTemplateColumns: "1fr 1fr"
    }
  },
  [theme.breakpoints.forTabletPortraitDown]:{
    root: {
      gridTemplateColumns: "1fr"
    }
  }
}));

const ProductsList = () => {
  const classes = useStyles();
  const [state] = useContext(Context);
  const { products } = state;

  return (
    <div className={classes.root}>
      {products.map(product => (
        <ProductItem key={product._id} product={product}/>
      ))}
    </div>
  );
}

export default ProductsList;
