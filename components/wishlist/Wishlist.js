import CloseIcon from '@material-ui/icons/Close';
import { Context } from "resources/Store";
import Drawer from '@material-ui/core/Drawer';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import WishlistItem from './WishlistItem';
import { makeStyles } from '@material-ui/core/styles';
import { useContext } from "react";

const useStyles = makeStyles(theme => ({
  root: {
    width: 350,
    maxWidth: "100%",
    padding:10
  },
  titleBar:{
    display:"flex",
    alignItems:"center"
  },
  title:{
    flexGrow:1
  },
  [theme.breakpoints.forTabletPortraitDown]:{
    root: {
      width: "auto",
      minWidth: 250
    }
  }
}));

const Wishlist = ({ open, onClose }) => {
  const classes = useStyles();
  const [ state ] = useContext(Context);
  const wishlistItems = Object.values(state.wishlist);

  return (
    <Drawer
      open={open}
      onClose={onClose}
      anchor="right"
    >
      <div className={classes.root}>
        <div className={classes.titleBar}>
          <Typography variant="h6" className={classes.title}>Wishlist</Typography>
          <IconButton onClick={onClose}>
            <CloseIcon/>
          </IconButton>
        </div>
        {!wishlistItems.length?
          <Typography variant="subtitle1" className={classes.title}>Wishlist empty</Typography>:false
        }
        {wishlistItems.map(item=>(
          <WishlistItem
            key={item._id}
            item={item}
          />
        ))}
      </div>
    </Drawer>
  );
}

export default Wishlist;
