import { changeWishlistItemQuantity, removeFromWishlist } from 'resources/actions';

import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CloseIcon from '@material-ui/icons/Close';
import { Context } from "resources/Store";
import IconButton from '@material-ui/core/IconButton';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { useContext } from "react";

const useStyles = makeStyles(() => ({
  root: {
    padding:3,
    marginBottom:5,
    display:"grid",
    gridTemplateColumns: "1fr 3fr",
    position:"relative"
  },
  media:{
    backgroundSize: "contain"
  },
  removeButton:{
    position:"absolute",
    right:3,
    top:3,
    background: "#fff"

  }
}));

const WishlistItem = ({ item }) => {
  const classes = useStyles();
  const [ state, dispatch ] = useContext(Context);

  const handleRemoveFromWishlist = () => {
    dispatch(removeFromWishlist(item));
  }

  const handleChangeQuantity = ({target}) => {
    const quantity = target.value;
    dispatch(changeWishlistItemQuantity(item, quantity));
  }

  return (
    <Card className={classes.root}>
      <IconButton
        size="small"
        className={classes.removeButton}
        onClick={handleRemoveFromWishlist}
      >
        <CloseIcon/>
      </IconButton>
      <CardMedia
        className={classes.media}
        image={item.img}
        title={item.name}
      />
      <div>
        <Typography variant="subtitle1">{item.name}</Typography>
        <div>
          <TextField
            label="Amount"
            value={item.quantity}
            type="number"
            onChange={handleChangeQuantity}
          />
        </div>
      </div>
    </Card>
  );
}

export default WishlistItem;
